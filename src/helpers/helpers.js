export const GetYearDifference = (year) => {

    return new Date().getFullYear() - year;

}

export const CalculateMark = (mark) => {

    let increase;

    switch (mark) {
        case 'american':
            increase = 1.30;
            break;

        case 'european':
            increase = 1.15;
            break;

        case 'asian':
            increase = 1.05;
            break;

        default:
            increase = 0;
            console.log('Default mark')
            break;
    }

    return increase;

}

export const GetPlan = (plan) => {

    return (plan === 'basic') ? 1.20 : 1.50;

}

export const FirstCapitalLetter = (text) => {

    return text.charAt(0).toUpperCase() + text.slice(1);

}