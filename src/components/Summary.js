import React from 'react';
import styled from "@emotion/styled";
import {FirstCapitalLetter} from "../helpers/helpers";
import PropTypes from "prop-types";

const ContentSummary = styled.div`
  padding: 1rem;
  text-align: center;
  background-color: #029F4E;
  color: #FFF;
  margin-top: 1rem;


  animation: fadeIn; /* referring directly to the animation's @keyframe declaration */
  animation-duration: 2s; /* don't forget to set a duration! */
`;

export const Summary = ({data}) => {

    const {mark, year, plan} = data;

    return (

        <ContentSummary>
            <h2>Quote Summary</h2>
            <ul>
                <li>Mark: {FirstCapitalLetter(mark)}</li>
                <li>Year: {FirstCapitalLetter(year)}</li>
                <li>Plan: {FirstCapitalLetter(plan)}</li>
            </ul>

        </ContentSummary>

    )
};

Summary.propTypes = {
    data: PropTypes.object.isRequired,
}