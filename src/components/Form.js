import React, {useState} from 'react';
import styled from "@emotion/styled";
import {CalculateMark, GetPlan, GetYearDifference} from "../helpers/helpers";
import PropTypes from "prop-types";

const FieldDiv = styled.div`
  display: flex;
  margin-bottom: 1rem;
  align-items: center;
`;

const Label = styled.label`
  flex: 0 0 100px;
`;

const Select = styled.select`
  display: block;
  width: 100%;
  padding: 1rem;
  border: 1px solid #e1e1e1;
  -webkit-appearance: none;
  outline: none;
`;

const InputRadio = styled.input`
  margin: 0 1rem;
`;

const Button = styled.button`
  background-color: #00838F;
  font-size: 16px;
  width: 100%;
  padding: 1rem;
  color: #fff;
  text-transform: uppercase;
  font-weight: bold;
  border: none;

  &:hover {
    background-color: #26C6DA;
    cursor: pointer;
    transition: background-color .3s ease;
  }
`;

const Error = styled.div`
  background-color: red;
  color: white;
  padding: 1rem;
  margin-bottom: 2rem;
  width: 94%;
  text-align: center;
`;


export const Form = ({setDataForm, setStartLoad}) => {

    const [data, setData] = useState({
        mark: '',
        year: '',
        plan: ''
    });

    const [error, setError] = useState(false);

    const {mark, year, plan} = data;

    const handleChange = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value
        });
    }

    const resetForm = () => {
        setData({
            mark: '',
            year: '',
            plan: '',
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        // to validate data
        if (data.plan.trim() === '' || data.year.trim() === '' || data.mark.trim() === '') {

            resetForm();
            setDataForm({});
            setError(true);
            return 1;
        }

        setError(false);
        let resultBase = 2000;

        // Difference year
        const difference = GetYearDifference(data.year);

        // for each year a 3 is subtracted
        resultBase -= ((difference * 3) * resultBase) / 100;

        // Difference mark
        resultBase = CalculateMark(data.mark) * resultBase;


        // Difference plan;
        const increasePlan = GetPlan(data.plan);
        resultBase = parseFloat((increasePlan * resultBase).toString()).toFixed(2);

        setDataForm({
            quotation: resultBase,
            data
        });

        setStartLoad(true);

        setTimeout(() => {
            setStartLoad(false);
        }, 1500);

        resetForm();

    }

    return (

        <form onSubmit={handleSubmit}>

            {error && <Error>The fields is required</Error>}

            <FieldDiv>

                <Label>Mark</Label>
                <Select name="mark" value={mark} onChange={handleChange}>

                    <option value=""> -- Choose --</option>
                    <option value="american">American</option>
                    <option value="european">European</option>
                    <option value="asian">Asian</option>

                </Select>

            </FieldDiv>

            <FieldDiv>

                <Label>Year</Label>
                <Select name="year" value={year} onChange={handleChange}>

                    <option value="">-- Choose --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>

                </Select>

            </FieldDiv>

            <FieldDiv>

                <Label>Plan</Label>

                <InputRadio type="radio" name="plan" id="basic" onChange={handleChange}
                            value="basic" checked={plan === "basic"}/>
                <Label htmlFor="basic">Basic</Label>

                <InputRadio type="radio" name="plan" id="full" onChange={handleChange}
                            value="full" checked={plan === "full"}/>
                <Label htmlFor="full">Full</Label>

            </FieldDiv>

            <Button>Quote</Button>

        </form>

    )
};

Form.propTypes = {
    setDataForm: PropTypes.func.isRequired,
    setStartLoad: PropTypes.func.isRequired,
}