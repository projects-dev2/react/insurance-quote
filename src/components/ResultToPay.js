import React from 'react';
import styled from "@emotion/styled";
import PropTypes from "prop-types";

const ParagraphResult = styled.p`
  background-color: rgb(127, 224, 237);
  margin-top: 1rem;
  padding: 1rem;
  text-align: center;

  animation: fadeIn; /* referring directly to the animation's @keyframe declaration */
  animation-duration: 2s; /* don't forget to set a duration! */
`;

export const ResultToPay = ({quotation}) => {

    return (

        <>
            <ParagraphResult>The total is: {quotation}</ParagraphResult>
        </>

    )
};

ResultToPay.propTypes = {
    quotation: PropTypes.string.isRequired,
}