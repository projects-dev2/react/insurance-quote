import React, {useState} from 'react';
import styled from "@emotion/styled";

import {Header} from "./components/Header";
import {Form} from "./components/Form";
import {Summary} from "./components/Summary";
import {ResultToPay} from "./components/ResultToPay";
import {Spinner} from "./components/Spinner";

const Container = styled.div`
  max-width: 600px;
  margin: 0 auto;
`;

const ContainerForm = styled.div`
  background-color: #FFF;
  padding: 3rem;
`;

export const App = () => {

    const [dataForm, setDataForm] = useState({});
    const [startLoad, setStartLoad] = useState(false);

    return (

        <Container>

            <Header title="Insurance Quote"/>

            <ContainerForm>
                <Form setDataForm={setDataForm} setStartLoad={setStartLoad}/>

                {(dataForm.quotation && !startLoad) &&
                    <div>
                        <Summary data={dataForm.data}/>
                        <ResultToPay quotation={dataForm.quotation} />
                    </div>
                }

                {startLoad && <Spinner/>}

            </ContainerForm>

        </Container>

    )
};